﻿using System;
using System.Collections.Generic;
using ToDoList.Services.Interfaces.Dtos;

namespace ToDoList.Services.Interfaces.Exceptions
{
    public class FeasibilityException : Exception
    {
        public IEnumerable<ReasonDto> Reasons { get; }

        public FeasibilityException(IEnumerable<ReasonDto> reasons)
        {
            Reasons = reasons;
        }
    }
}
