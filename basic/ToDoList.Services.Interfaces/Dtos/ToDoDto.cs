﻿namespace ToDoList.Services.Interfaces.Dtos
{
    public class ToDoDto
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public bool Done { get; set; }
    }
}
