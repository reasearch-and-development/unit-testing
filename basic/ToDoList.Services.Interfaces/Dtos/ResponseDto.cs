﻿namespace ToDoList.Services.Interfaces.Dtos
{
    public class ResponseDto<T>
    {
        public T Item { get; set; }
    }
}
