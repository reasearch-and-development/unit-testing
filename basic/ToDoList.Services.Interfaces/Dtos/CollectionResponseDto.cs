﻿using System.Collections.Generic;
using System.Linq;

namespace ToDoList.Services.Interfaces.Dtos
{
    public class CollectionResponseDto<TItem> : ResponseDto<IEnumerable<TItem>>
    {
        public int Count => Item.Count();
    }
}
