﻿using System.Collections.Generic;

namespace ToDoList.Services.Interfaces.Dtos
{
    public class ReasonDto
    {
        public int Code { get; set; }
        public IEnumerable<object> Contents { get; set; }

        public static class Codes
        {
            public const int MandatoryField = 1;
			public const int InvalidObject = 2;
		}
    }
}
