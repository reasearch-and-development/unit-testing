﻿using System.Collections.Generic;
using ToDoList.Services.Interfaces.Dtos;

namespace ToDoList.Services.Interfaces
{
    public interface IToDoValidationService
    {
        /// <summary>
        ///     Validates a ToDo item.
        /// </summary>
        /// <param name="toDo">ToDo item to validate.</param>
        IEnumerable<ReasonDto> Validate(ToDoDto toDo);
    }
}
