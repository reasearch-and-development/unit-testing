﻿namespace ToDoList.Services.Interfaces
{
    public interface IConfigurationProvider
    {
        string StoragePath { get; }
    }
}
