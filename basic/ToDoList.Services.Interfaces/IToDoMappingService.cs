﻿using ToDoList.Entities;
using ToDoList.Services.Interfaces.Dtos;

namespace ToDoList.Services.Interfaces
{
    public interface IToDoMappingService
    {
        /// <summary>
        ///     Maps a ToDo item from a ToDoDto.
        /// </summary>
        /// <param name="dto">Mapping from.</param>
        /// <returns>Returns a ToDo item, if dto is null returns null.</returns>
        ToDo FromDto(ToDoDto dto);

        /// <summary>
        ///     Maps a ToDoDto from a ToDo item.
        /// </summary>
        /// <param name="todo">Mapping from</param>
        /// <returns>Returns a ToDoDto item, if todo is null returns null.</returns>
        ToDoDto ToDto(ToDo todo);
    }
}
