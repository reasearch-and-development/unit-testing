﻿using System.Collections.Generic;
using ToDoList.Services.Interfaces.Dtos;

namespace ToDoList.Services.Interfaces
{
    public interface IToDoService
    {
        /// <summary>
        ///     Save a ToDo item.
        /// </summary>
        /// <param name="toDo">ToDo item to save.</param>
        /// <returns>Returns the saved ToDo item.</returns>
        ToDoDto Save(ToDoDto toDo);

        /// <summary>
        ///     Delete a ToDo item having toDoId as id.
        /// </summary>
        /// <param name="toDoId">ToDo item to delete id.</param>
        /// <returns>Returns true if the ToDo item was deleted, otherwise false.</returns>
        bool Delete(int toDoId);

        /// <summary>
        ///     Find an existing ToDo item having toDoId as id.
        /// </summary>
        /// <param name="toDoId">ToDo item to get id.</param>
        /// <returns>Returns a ToDo item having toDoId as id, null if it does not exist.</returns>
        ToDoDto Find(int toDoId);

        /// <summary>
        ///     Fomd all existing ToDo items.
        /// </summary>
        /// <returns>Returns all existing ToDo items.</returns>
        IEnumerable<ToDoDto> FindAll();
    }
}
