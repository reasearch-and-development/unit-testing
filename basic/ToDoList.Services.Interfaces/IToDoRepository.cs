﻿using System.Collections.Generic;
using ToDoList.Entities;

namespace ToDoList.Services.Interfaces
{
    public interface IToDoRepository
    {
        /// <summary>
        ///     Add a new ToDo item.
        /// </summary>
        /// <param name="toDo">ToDo item to add.</param>
        /// <returns>Returns the added ToDo item.</returns>
        ToDo Add(ToDo toDo);

        /// <summary>
        ///     Delete a ToDo item having toDoId as id.
        /// </summary>
        /// <param name="toDoId">ToDo item to delete id.</param>
        /// <returns>Returns true if the ToDo item was deleted, otherwise false.</returns>
        bool Delete(int toDoId);

        /// <summary>
        ///     Update an existing ToDo item.
        /// </summary>
        /// <param name="toDo">ToDo item to update.</param>
        /// <returns>Returns the updated ToDo item.</returns>
        ToDo Update(ToDo toDo);

        /// <summary>
        ///     Get an existing ToDo item having toDoId as id.
        /// </summary>
        /// <param name="toDoId">ToDo item to get id.</param>
        /// <returns>Returns a ToDo item having toDoId as id, null if it does not exist.</returns>
        ToDo Get(int toDoId);

        /// <summary>
        ///     Get all existing ToDo items.
        /// </summary>
        /// <returns>Returns all existing ToDo items.</returns>
        IEnumerable<ToDo> GetAll();
    }
}
