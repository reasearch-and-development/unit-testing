﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Web.Http;
using ToDoList.Services.Interfaces;
using ToDoList.Services.Interfaces.Dtos;
using ToDoList.Services.Interfaces.Exceptions;

namespace ToDoList.Controllers
{
    public class ToDoController : ApiController
    {
        private readonly IToDoService toDoService;

        public ToDoController(IToDoService toDoService)
        {
            this.toDoService = toDoService;
        }

        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            var foundToDo = toDoService.Find(id);
            if (foundToDo == null)
            {
                return NotFound();
            }

            return Ok(Create(foundToDo));
        }

        [HttpGet]
        public IHttpActionResult Get()
        {
            var foundToDos = toDoService.FindAll();
            return Ok(Create(foundToDos));
        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            var deleted = toDoService.Delete(id);
            return Ok(Create(deleted));
        }

        [HttpPost]
        public IHttpActionResult Save(ToDoDto dto)
        {
            ToDoDto saved = toDoService.Save(dto);
            return Ok(Create(saved));
        }

        private static ResponseDto<T> Create<T>(T item)
        {
            return new ResponseDto<T>
            {
                Item = item
            };
        }

        private static CollectionResponseDto<T> Create<T>(IEnumerable<T> item)
        {
            return new CollectionResponseDto<T>
            {
                Item = item
            };
        }
    }
}
