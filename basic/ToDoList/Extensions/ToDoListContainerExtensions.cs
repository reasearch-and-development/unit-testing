﻿using System;
using System.ComponentModel.Composition;
using System.Linq;
using System.Reflection;
using SimpleInjector;

namespace ToDoList.Extensions
{
    public static class ToDoListContainerExtensions
    {
        public static void RegisterApplicationServices(this Container container)
        {
            AppDomain.CurrentDomain
                .GetAssemblies()
                .SelectMany(x => x.GetTypes())
                .Where(x => Attribute.IsDefined(x, typeof(ExportAttribute)))
                .ToList()
                .ForEach(x => Register(container, x));
        }

        private static void Register(Container container, Type attributedType)
        {
            ExportAttribute export = attributedType.GetCustomAttribute<ExportAttribute>();

            Type contractType = export.ContractType;
            Type concreteType = attributedType;

            if (Attribute.IsDefined(attributedType, typeof(PartCreationPolicyAttribute)))
            {
                RegisterWithPartCreationPolicy(container, attributedType, contractType, concreteType);
            }
            else
            {
                RegisterWithoutPartCreationPolicy(container, contractType, concreteType);
            }
        }

        private static void RegisterWithoutPartCreationPolicy(Container container, Type contractType, Type concreteType)
        {
            container.Register(contractType, concreteType, container.Options.DefaultScopedLifestyle);
        }

        private static void RegisterWithPartCreationPolicy(Container container, Type attributedType, Type contractType, Type concreteType)
        {
            PartCreationPolicyAttribute partCreationPolicy = attributedType.GetCustomAttribute<PartCreationPolicyAttribute>();

            switch (partCreationPolicy.CreationPolicy)
            {
                case CreationPolicy.Shared:
                    container.RegisterSingleton(contractType, concreteType);
                    break;

                case CreationPolicy.NonShared:
                    container.Register(contractType, concreteType);
                    break;

                default:
                    container.Register(contractType, concreteType, container.Options.DefaultScopedLifestyle);
                    break;
            }
        }
    }
}
