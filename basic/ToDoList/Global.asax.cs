﻿using System;
using System.Web;
using System.Web.Http;

namespace ToDoList
{
    public class Global : HttpApplication
    {
        private void Application_Start(object sender, EventArgs e)
        {
            ContainerConfig.Register();
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }
    }
}
