﻿using System.Web.Http;
using SimpleInjector;
using SimpleInjector.Integration.Web;
using SimpleInjector.Integration.WebApi;
using ToDoList.Extensions;

namespace ToDoList
{
    public static class ContainerConfig
    {
        public static void Register()
        {
            var container = new Container();
            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();
            container.RegisterWebApiControllers(GlobalConfiguration.Configuration);
            container.RegisterApplicationServices();
            container.Verify();
            GlobalConfiguration.Configuration.DependencyResolver = new SimpleInjectorWebApiDependencyResolver(container);
        }
    }
}
