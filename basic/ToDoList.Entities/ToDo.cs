﻿namespace ToDoList.Entities
{
    public class ToDo
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public bool Done { get; set; }

        public bool IsTransient()
        {
            return Id <= 0;
        }
    }
}
