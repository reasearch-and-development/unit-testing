﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using ToDoList.Entities;
using ToDoList.Services.Interfaces;

namespace ToDoList.Services.Impl
{
    [Export(typeof(IToDoRepository))]
    internal class ToDoRepository : IToDoRepository
    {
        private const int StartingId = 0;
        private const string ToDoNameIdRegex = @"ToDo_\d*.json$";

        private readonly IConfigurationProvider configurationProvider;

        public ToDoRepository(IConfigurationProvider configurationProvider)
        {
            this.configurationProvider = configurationProvider;
        }

        public ToDo Add(ToDo toDo)
        {
            if (toDo == null)
            {
                throw new ArgumentNullException(nameof(toDo));
            }

            CheckTransient(toDo);
            ToDo adding = GenerateToDo(toDo);
            CheckToDoNotExist(adding);
            CreateFile(adding);

            return adding;
        }

        public bool Delete(int toDoId)
        {
            ToDo deleting = new ToDo { Id = toDoId };

            string file = GetFileName(deleting);
            if (!File.Exists(file))
            {
                return false;
            }

            File.Delete(file);
            return true;
        }

        public ToDo Update(ToDo toDo)
        {
            if (toDo == null)
            {
                throw new ArgumentNullException(nameof(toDo));
            }

            if (!File.Exists(GetFileName(toDo)))
            {
                throw new ArgumentException();
            }

            ToDo updating = GenerateToDo(toDo);
            CreateFile(updating);

            return updating;
        }

        public ToDo Get(int toDoId)
        {
            ToDo toDo = new ToDo
            {
                Id = toDoId
            };

            string file = GetFileName(toDo);
            if (!File.Exists(file))
            {
                return null;
            }

            return JsonConvert.DeserializeObject<ToDo>(File.ReadAllText(file));
        }

        public IEnumerable<ToDo> GetAll()
        {
            return EnumerateToDoIds()
                .Select(Get)
                .ToList();
        }

        private string GetFileName(ToDo toDo)
        {
            return Path.Combine(configurationProvider.StoragePath, $"ToDo_{toDo.Id}.json");
        }

        private void CheckToDoNotExist(ToDo toDo)
        {
            string filename = GetFileName(toDo);
            if (File.Exists(filename))
            {
                throw new InvalidOperationException("To Do item already exists");
            }
        }

        private void CreateFile(ToDo toDo)
        {
            string filename = GetFileName(toDo);
            string json = JsonConvert.SerializeObject(toDo);
            File.WriteAllText(filename, json);
        }

        private ToDo GenerateToDo(ToDo toDo)
        {
            if (toDo == null)
            {
                return null;
            }

            if (toDo.IsTransient())
            {
                toDo.Id = NextId();
            }

            return toDo;
        }

        private int NextId()
        {
            return EnumerateToDoIds()
                .Union(new[] { StartingId })
                .Max() + 1;
        }

        private static void CheckTransient(ToDo toDo)
        {
            if (!toDo.IsTransient())
            {
                throw new ArgumentException();
            }
        }

        private IEnumerable<int> EnumerateToDoIds()
        {
            return new DirectoryInfo(configurationProvider.StoragePath)
                .EnumerateFiles("ToDo_*.json")
                .Select(x => x.Name)
                .Where(x => Regex.IsMatch(x, ToDoNameIdRegex))
                .Select(x => x.Substring("ToDo_".Length, x.Length - ("ToDo_".Length + ".json".Length)))
                .Select(int.Parse);
        }
    }
}
