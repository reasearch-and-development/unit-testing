﻿using System.ComponentModel.Composition;
using ToDoList.Entities;
using ToDoList.Services.Interfaces;
using ToDoList.Services.Interfaces.Dtos;

namespace ToDoList.Services.Impl
{
    [Export(typeof(IToDoMappingService))]
    internal class ToDoMappingService : IToDoMappingService
    {
        public ToDo FromDto(ToDoDto dto)
        {
            if (dto == null)
            {
                return null;
            }

            return new ToDo
            {
                Id = dto.Id,
                Done = dto.Done,
                Text = dto.Text
            };
        }

        public ToDoDto ToDto(ToDo todo)
        {
            if (todo == null)
            {
                return null;
            }

            return new ToDoDto
            {
                Id = todo.Id,
                Done = todo.Done,
                Text = todo.Text
            };
        }
    }
}
