﻿using System.ComponentModel.Composition;
using System.Configuration;
using ToDoList.Services.Interfaces;

namespace ToDoList.Services.Impl
{
    [Export(typeof(IConfigurationProvider))]
    internal class ConfigurationProvider : IConfigurationProvider
    {
        public string StoragePath => ConfigurationManager.AppSettings["StoragePath"];
    }
}
