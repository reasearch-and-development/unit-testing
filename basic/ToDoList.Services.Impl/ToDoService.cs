﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using ToDoList.Entities;
using ToDoList.Services.Interfaces;
using ToDoList.Services.Interfaces.Dtos;
using ToDoList.Services.Interfaces.Exceptions;

namespace ToDoList.Services.Impl
{
    [Export(typeof(IToDoService))]
    internal class ToDoService : IToDoService
    {
        private readonly IToDoValidationService mValidator;
        private readonly IToDoMappingService mMapper;
        private readonly IToDoRepository mToDoRepository;

        public ToDoService(
            IToDoValidationService validator,
            IToDoMappingService mapper,
            IToDoRepository toDoRepository)
        {
            mValidator = validator;
            mMapper = mapper;
            mToDoRepository = toDoRepository;
        }

        public bool Delete(int toDoId)
        {
            return mToDoRepository.Delete(toDoId);
        }

        public ToDoDto Find(int toDoId)
        {
            return mMapper.ToDto(mToDoRepository.Get(toDoId));
        }

        public IEnumerable<ToDoDto> FindAll()
        {
            return mToDoRepository
                .GetAll()
                .Select(mMapper.ToDto)
                .ToList();
        }

        public ToDoDto Save(ToDoDto toDo)
        {
            IEnumerable<ReasonDto> reasons = mValidator.Validate(toDo);
            if (reasons.Any())
            {
                throw new FeasibilityException(reasons);
            }

            ToDo model = mMapper.FromDto(toDo);
            if (model.IsTransient())
            {
                model = mToDoRepository.Add(model);
            }
            else
            {
                model = mToDoRepository.Update(model);
            }

            return mMapper.ToDto(model);
        }
    }
}
