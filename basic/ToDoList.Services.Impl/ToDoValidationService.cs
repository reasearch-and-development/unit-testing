﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using ToDoList.Services.Interfaces;
using ToDoList.Services.Interfaces.Dtos;

namespace ToDoList.Services.Impl
{
    [Export(typeof(IToDoValidationService))]
    internal class ToDoValidationService : IToDoValidationService
    {
        public IEnumerable<ReasonDto> Validate(ToDoDto toDo)
        {
            if (toDo == null)
            {
                return new[] {
                    new ReasonDto {
                        Code = ReasonDto.Codes.InvalidObject,
                        Contents = new object[0]
                    }
                };
            }

            IList<ReasonDto> reasons = new List<ReasonDto>();
            if (string.IsNullOrEmpty(toDo.Text))
            {
                reasons.Add(
                    new ReasonDto
                    {
                        Code = ReasonDto.Codes.MandatoryField,
                        Contents = new[] { nameof(ToDoDto.Text) }
                    }
                );
            }

            return reasons;
        }
    }
}
