﻿using System;
using NUnit.Framework;

namespace ToDoList.Entities.Tests
{
    [TestFixture, Category("Standalone"), TestOf(typeof(ToDo))]
    internal class ToDoTest
    {
        private static readonly Random Random = new Random();

        [Test]
        public void IsTransient_IdPositive_ReturnsFalse([Values(1, null)] int? idHint)
        {
            int id = idHint ?? Random.Next(2, int.MaxValue);
            ToDo sut = new ToDo() { Id = id };

            Assert.That(sut.IsTransient(), Is.False);
        }

        [Test]
        public void IsTransient_IdNonPositive_ReturnsTrue([Values(0, -1, null)] int? idHint)
        {
            int id = idHint ?? Random.Next(int.MinValue, -1);
            ToDo sut = new ToDo() { Id = id };

            Assert.That(sut.IsTransient(), Is.True);
        }
    }
}
