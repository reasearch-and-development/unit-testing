﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using Moq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using ToDoList.Entities;
using ToDoList.Services.Impl;
using ToDoList.Services.Interfaces;

namespace ToDoList.Services.Tests
{
    [TestFixture, Category("Integrated"), TestOf(typeof(ToDoRepository))]
    internal class ToDoRepositoryTest
    {
        private readonly string storagePath;
        private IDictionary<int, JToken> knownFiles;
        private int nextId;

        private Mock<IConfigurationProvider> configurationProviderMock;
        private IToDoRepository sut;

        public ToDoRepositoryTest()
        {
            // ReSharper disable once AssignNullToNotNullAttribute
            storagePath = Path.Combine(Path.GetTempPath(), typeof(ToDoRepositoryTest).FullName);
        }

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            IEnumerable<int> GenerateRandomFileIds()
            {
                int count = ProjectUtils.Random.Next(1, 5);
                for (int i = 0; i < count; i++)
                {
                    yield return ProjectUtils.Random.Next();
                }
            }

            knownFiles = GenerateRandomFileIds().Distinct().ToDictionary(x => x, x => (JToken)null);
            nextId = knownFiles.Keys.Max() + 1;
        }

        [SetUp]
        public void SetUp()
        {
            SetUpFilledFolder();

            configurationProviderMock = new Mock<IConfigurationProvider>(MockBehavior.Strict);
            configurationProviderMock.SetupGet(x => x.StoragePath).Returns(storagePath);

            sut = new ToDoRepository(
                configurationProviderMock.Object
            );
        }

        [TearDown]
        public void TearDown()
        {
            TearDownFolder();
        }

        [Test]
        public void Add_IsNull_ThrowsArgumentNullException()
        {
            Assert.That(() => sut.Add(null), Throws.ArgumentNullException);
        }

        [Test]
        public void Add_IsTransientAndWithoutOtherInstances_CreatesNewFileWithId1()
        {
            SetUpEmptyFolder();
            Add_IsTransient_CreatesNewFile(1);
        }

        [Test]
        public void Add_IsTransientAndWithOtherInstances_CreatesNewFileWithNextId()
        {
            Add_IsTransient_CreatesNewFile(nextId);
        }

        private void Add_IsTransient_CreatesNewFile(int id)
        {
            ToDo item = new ToDo()
            {
                Id = 0,
                Text = $"Text {ProjectUtils.Random.Next():x8}",
                Done = ProjectUtils.Random.Next(2) == 1,
            };
            ToDo expected = new ToDo()
            {
                Id = id,
                Text = item.Text,
                Done = item.Done,
            };

            ToDo actual = sut.Add(item);

            Assert.That(actual, Is.EqualTo(expected).Using(ToDoEqualityComparer.Instance));
            AssertFileCompilant(expected);
        }

        [Test]
        public void Add_IsNotTransient_ThrowsArgumentException()
        {
            ToDo item = new ToDo() { Id = ProjectUtils.Random.Next(1, int.MaxValue) };

            Assert.That(() => sut.Add(item), Throws.ArgumentException);
        }

        [Test]
        public void Delete_IdDoesNotExist_ReturnsFalse()
        {
            int id = GetRandomNonExistingId();

            bool actual = sut.Delete(id);

            Assert.That(actual, Is.False);
        }

        [Test]
        public void Delete_IdExists_DeletesFileAndReturnsTrue()
        {
            int id = GetRandomExistingId();

            bool actual = sut.Delete(id);

            Assert.That(actual, Is.True);
            Assert.That(new FileInfo(GetFilePath(id)), Does.Not.Exist);
        }

        [Test]
        public void Update_IsNull_ThrowsArgumentNullException()
        {
            Assert.That(() => sut.Update(null), Throws.ArgumentNullException);
        }

        [Test]
        public void Update_IdDoesNotExist_ThrowsArgumentException()
        {
            ToDo item = new ToDo() { Id = GetRandomNonExistingId() };

            Assert.That(() => sut.Update(item), Throws.ArgumentException);
        }

        [Test]
        public void Update_IdExists_SavesAndReturnsIt()
        {
            ToDo item = new ToDo()
            {
                Id = GetRandomExistingId(),
                Text = $"Text {ProjectUtils.Random.Next():x8}",
                Done = ProjectUtils.Random.Next(2) == 1,
            };
            ToDo expected = item;

            ToDo actual = sut.Update(item);

            Assert.That(actual, Is.EqualTo(expected).Using(ToDoEqualityComparer.Instance));
            AssertFileCompilant(expected);
        }

        [Test]
        public void Get_IdDoesNotExist_ReturnsNull()
        {
            int id = GetRandomNonExistingId();

            ToDo actual = sut.Get(id);

            Assert.That(actual, Is.Null);
        }

        [Test]
        public void Get_IdExists_ReadsFile()
        {
            int id = GetRandomExistingId();
            ToDo expected = knownFiles[id].ToObject<ToDo>();

            ToDo actual = sut.Get(id);

            Assert.That(actual, Is.EqualTo(expected).Using(ToDoEqualityComparer.Instance));
        }

        [Test]
        public void GetAll__ReturnsTokensConvertedAsItem()
        {
            IEnumerable<ToDo> expected = knownFiles.Values.Select(x => x.ToObject<ToDo>()).ToList();

            IEnumerable<ToDo> actual = sut.GetAll();

            Assert.That(actual, Is.EquivalentTo(expected).Using(ToDoEqualityComparer.Instance));
        }

        private void SetUpFilledFolder()
        {
            SetUpEmptyFolder();
            foreach (int id in knownFiles.Keys.ToList())
            {
                CreateRandomFile(id);
            }
        }

        private void SetUpEmptyFolder()
        {
            TearDownFolder();
            Directory.CreateDirectory(storagePath);
        }

        private void TearDownFolder()
        {
            if (Directory.Exists(storagePath))
            {
                Directory.Delete(storagePath, true);
            }
        }

        private void CreateRandomFile(int id)
        {
            JToken token = new JObject()
            {
                { nameof(ToDo.Id), id },
                { nameof(ToDo.Text), $"Text {ProjectUtils.Random.Next():x8}" },
                { nameof(ToDo.Done), ProjectUtils.Random.Next(2) == 1 },
            };

            knownFiles[id] = token;

            File.WriteAllText(GetFilePath(id), token.ToString(Formatting.None));
        }

        private string GetFilePath(int id)
        {
            return Path.Combine(storagePath, string.Format(CultureInfo.InvariantCulture, "ToDo_{0:D}.json", id));
        }

        private void AssertFileCompilant(ToDo expected)
        {
            string filePath = GetFilePath(expected.Id);
            Assert.That(new FileInfo(filePath), Does.Exist);

            JToken actual;
            try
            {
                actual = JToken.Parse(File.ReadAllText(filePath));
            }
            catch (JsonSerializationException exception)
            {
                throw new AssertionException("parsing as JSON failed", exception);
            }
            Assert.That(actual, Is.EqualTo(JToken.FromObject(expected)).Using(JToken.EqualityComparer));
        }

        private int GetRandomNonExistingId()
        {
            while (true)
            {
                int id = ProjectUtils.Random.Next(int.MinValue, int.MaxValue);
                if (!knownFiles.ContainsKey(id))
                {
                    return id;
                }
            }
        }

        private int GetRandomExistingId()
        {
            return knownFiles.Keys.ElementAt(ProjectUtils.Random.Next(knownFiles.Count));
        }

        private class ToDoEqualityComparer : IEqualityComparer<ToDo>
        {
            public static readonly IEqualityComparer<ToDo> Instance = new ToDoEqualityComparer();

            private ToDoEqualityComparer() { }

            bool IEqualityComparer<ToDo>.Equals(ToDo x, ToDo y)
            {
                if (x == null && y == null)
                    return true;

                if (x == null || y == null)
                    return false;

                return x.Id == y.Id
                    && x.Text == y.Text
                    && x.Done == y.Done;
            }

            int IEqualityComparer<ToDo>.GetHashCode(ToDo obj)
            {
                throw new NotSupportedException();
            }
        }
    }
}
