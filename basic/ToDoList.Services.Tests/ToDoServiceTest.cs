﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Moq;
using NUnit.Framework;
using ToDoList.Entities;
using ToDoList.Services.Impl;
using ToDoList.Services.Interfaces;
using ToDoList.Services.Interfaces.Dtos;
using ToDoList.Services.Interfaces.Exceptions;

namespace ToDoList.Services.Tests
{
    [TestFixture, Category("Standalone"), TestOf(typeof(ToDoService))]
    internal class ToDoServiceTest
    {
        private Mock<IToDoValidationService> validatorMock;
        private Mock<IToDoMappingService> mapperMock;
        private Mock<IToDoRepository> toDoRepositoryMock;
        private IToDoService sut;

        [SetUp]
        public void SetUp()
        {
            validatorMock = new Mock<IToDoValidationService>(MockBehavior.Strict);
            mapperMock = new Mock<IToDoMappingService>(MockBehavior.Strict);
            toDoRepositoryMock = new Mock<IToDoRepository>(MockBehavior.Strict);

            sut = new ToDoService(
                validatorMock.Object,
                mapperMock.Object,
                toDoRepositoryMock.Object
            );
        }

        [Test]
        public void Delete__ForwardsToRepository([Values] bool expected)
        {
            int toDoId = ProjectUtils.Random.Next();
            toDoRepositoryMock.Setup(x => x.Delete(toDoId)).Returns(expected);

            bool actual = sut.Delete(toDoId);

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public void Find_ItemDoesNotExist_AsksRepositoryAndReturnsMapped()
        {
            Find__AsksRepositoryAndReturnsMapped();
        }

        [Test]
        public void Find_ItemExists_AsksRepositoryAndReturnsMapped()
        {
            Find__AsksRepositoryAndReturnsMapped();
        }

        private void Find__AsksRepositoryAndReturnsMapped()
        {
            int toDoId = ProjectUtils.Random.Next();
            ToDo item = new ToDo();
            ToDoDto expected = new ToDoDto();

            toDoRepositoryMock.Setup(x => x.Get(toDoId)).Returns(item);
            mapperMock.Setup(x => x.ToDto(item)).Returns(expected);

            ToDoDto actual = sut.Find(toDoId);

            Assert.That(actual, Is.SameAs(expected));
        }

        [Test]
        public void FindAll__AsksRepositoryAndReturnsMapped()
        {
            int itemCount = ProjectUtils.Random.Next(0, 10);

            IDictionary<ToDo, ToDoDto> dict = new Dictionary<ToDo, ToDoDto>();
            for (int i = 0; i < itemCount; i++)
            {
                dict.Add(new ToDo(), new ToDoDto());
            }

            toDoRepositoryMock.Setup(x => x.GetAll()).Returns(dict.Keys);
            mapperMock.Setup(x => x.ToDto(It.IsAny<ToDo>())).Returns((ToDo item) => dict[item]);

            IEnumerable<ToDoDto> actual = sut.FindAll();

            Assert.That(actual, Is.EquivalentTo(dict.Values));
        }

        [Test]
        public void Save_ValidationFails_ThrowsValidationException()
        {
            int reasonsCount = ProjectUtils.Random.Next(1, 10);

            ToDoDto dto = new ToDoDto();
            IEnumerable<ReasonDto> reasons = MakeRandomReasons().ToList();

            IEnumerable<ReasonDto> MakeRandomReasons()
            {
                for (int i = 0; i < reasonsCount; i++)
                {
                    yield return new ReasonDto() { Code = ProjectUtils.Random.Next() };
                }
            }

            validatorMock.Setup(x => x.Validate(dto)).Returns(reasons);

            Assert.That(() => sut.Save(dto), Throws.InstanceOf<FeasibilityException>());
        }

        [Test]
        public void Save_ModelIsTransient_MapAddAndMapBack()
        {
            Save__MapPersistAndMapBack(0, item => x => x.Add(item));
        }

        [Test]
        public void Save_ModelIsNotTransient_MapUpdateAndMapBack()
        {
            Save__MapPersistAndMapBack(42, item => x => x.Update(item));
        }

        private void Save__MapPersistAndMapBack(int itemId, Func<ToDo, Expression<Func<IToDoRepository, ToDo>>> repositoryExprFunc)
        {
            ToDoDto dto = new ToDoDto();
            ToDo item = new ToDo() { Id = itemId };
            ToDo persistedItem = new ToDo();
            ToDoDto expected = new ToDoDto();

            validatorMock.Setup(x => x.Validate(dto)).Returns(Enumerable.Empty<ReasonDto>());
            mapperMock.Setup(x => x.FromDto(dto)).Returns(item);
            toDoRepositoryMock.Setup(repositoryExprFunc(item)).Returns(persistedItem);
            mapperMock.Setup(x => x.ToDto(persistedItem)).Returns(expected);

            ToDoDto actual = sut.Save(dto);

            Assert.That(actual, Is.SameAs(expected));
        }
    }
}
