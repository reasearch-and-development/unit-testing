﻿using System;
using NUnit.Framework;
using ToDoList.Entities;
using ToDoList.Services.Impl;
using ToDoList.Services.Interfaces;
using ToDoList.Services.Interfaces.Dtos;

namespace ToDoList.Services.Tests
{
    [TestFixture, Category("Standalone"), TestOf(typeof(ToDoMappingService))]
    internal class ToDoMappingServiceTest
    {
        private IToDoMappingService sut;

        [SetUp]
        public void SetUp()
        {
            sut = new ToDoMappingService();
        }

        [Test]
        public void FormDto_InputNull_ReturnsNull()
        {
            ToDo actual = sut.FromDto(null);

            Assert.That(actual, Is.Null);
        }

        [Test]
        public void FromDto_InputNotNull_ReturnsExpected()
        {
            int id = ProjectUtils.Random.Next();
            string text = $"Text {ProjectUtils.Random.Next():X8}";
            bool done = ProjectUtils.Random.Next(2) == 1;

            ToDoDto dto = new ToDoDto()
            {
                Id = id,
                Text = text,
                Done = done,
            };

            ToDo actual = sut.FromDto(dto);

            Assert.That(actual, Is.Not.Null);
            Assert.That(actual.Id, Is.EqualTo(id));
            Assert.That(actual.Text, Is.EqualTo(text));
            Assert.That(actual.Done, Is.EqualTo(done));
        }

        [Test]
        public void ToDto_InputNull_ReturnsNull()
        {
            ToDoDto actual = sut.ToDto(null);

            Assert.That(actual, Is.Null);
        }

        [Test]
        public void ToDto_InputNotNull_ReturnsExpected()
        {
            int id = ProjectUtils.Random.Next();
            string text = $"Text {ProjectUtils.Random.Next():X8}";
            bool done = ProjectUtils.Random.Next(2) == 1;

            ToDo item = new ToDo()
            {
                Id = id,
                Text = text,
                Done = done,
            };

            ToDoDto actual = sut.ToDto(item);

            Assert.That(actual, Is.Not.Null);
            Assert.That(actual.Id, Is.EqualTo(id));
            Assert.That(actual.Text, Is.EqualTo(text));
            Assert.That(actual.Done, Is.EqualTo(done));
        }
    }
}
