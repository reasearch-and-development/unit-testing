﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using ToDoList.Services.Impl;
using ToDoList.Services.Interfaces;
using ToDoList.Services.Interfaces.Dtos;

namespace ToDoList.Services.Tests
{
    [TestFixture, Category("Standalone"), TestOf(typeof(ToDoValidationService))]
    internal class ToDoValidationServiceTest
    {
        private IToDoValidationService sut;

        [SetUp]
        public void SetUp()
        {
            sut = new ToDoValidationService();
        }

        [Test]
        public void Validate_ArgumentNull_Returns1ReasonInvalidObject()
        {
            IEnumerable<ReasonDto> actual = sut.Validate(null).ToList();

            Assert.That(actual, Is.Not.Null);
            Assert.That(actual.Count(), Is.EqualTo(1));
            Assert.That(
                actual,
                Has.One.Matches(
                    Has.Property(nameof(ReasonDto.Code)).EqualTo(ReasonDto.Codes.InvalidObject)
                    & Has.Property(nameof(ReasonDto.Contents)).EqualTo(new object[0])
                )
            );
        }

        [Test]
        public void Validate_TextNullOrEmpty_Returns1ReasonMandatoryField([Values(null, "")] string text)
        {
            ToDoDto dto = new ToDoDto() { Text = text };

            IEnumerable<ReasonDto> actual = sut.Validate(dto).ToList();

            Assert.That(actual, Is.Not.Null);
            Assert.That(actual.Count(), Is.EqualTo(1));
            Assert.That(
                actual,
                Has.One.Matches(
                    Has.Property(nameof(ReasonDto.Code)).EqualTo(ReasonDto.Codes.MandatoryField)
                    & Has.Property(nameof(ReasonDto.Contents)).EqualTo(new[] { nameof(ToDoDto.Text) })
                )
            );
        }

        [Test]
        public void Validate_TextNotNullOrEmpty_ReturnsEmpty()
        {
            ToDoDto dto = new ToDoDto() { Text = $"Text {ProjectUtils.Random.Next():x8}" };

            IEnumerable<ReasonDto> actual = sut.Validate(dto).ToList();

            Assert.That(actual, Is.Not.Null);
            Assert.That(actual.Count(), Is.EqualTo(0));
        }
    }
}
