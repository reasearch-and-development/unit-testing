﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Results;
using Moq;
using NUnit.Framework;
using ToDoList.Controllers;
using ToDoList.Services.Interfaces;
using ToDoList.Services.Interfaces.Dtos;

namespace ToDoList.Web.Tests
{
    [TestFixture, Category("Standalone"), TestOf(typeof(ToDoController))]
    internal class ToDoControllerTest
    {
        private static readonly Random Random = new Random();

        private Mock<IToDoService> toDoServiceMock;
        private ToDoController sut;

        [SetUp]
        public void SetUp()
        {
            toDoServiceMock = new Mock<IToDoService>(MockBehavior.Strict);

            sut = new ToDoController(toDoServiceMock.Object);
        }

        [Test]
        public void Getà1_FindReturnsNotNull_ReturnsOkResponseWithSuchContent()
        {
            int id = Random.Next();
            ToDoDto dto = new ToDoDto();

            toDoServiceMock.Setup(x => x.Find(id)).Returns(dto);

            IHttpActionResult actual = sut.Get(id);

            Assert.That(actual, Is.InstanceOf<OkNegotiatedContentResult<ResponseDto<ToDoDto>>>());

            var readableActual = (OkNegotiatedContentResult<ResponseDto<ToDoDto>>)actual;

            Assert.That(readableActual.Content, Is.Not.Null);
            Assert.That(readableActual.Content.Item, Is.SameAs(dto));
        }

        [Test]
        public void Getà1_FindReturnsNull_ReturnsNotFoundResponse()
        {
            int id = Random.Next();

            toDoServiceMock.Setup(x => x.Find(id)).Returns((ToDoDto)null);

            IHttpActionResult actual = sut.Get(id);

            Assert.That(actual, Is.InstanceOf<NotFoundResult>());
        }

        [Test]
        public void Getà2__ReturnsOkResponseWithContentFromFindAll()
        {
            int itemCount = Random.Next(0, 10);

            ICollection<ToDoDto> coll = new List<ToDoDto>();
            for (int i = 0; i < itemCount; i++)
            {
                coll.Add(new ToDoDto());
            }

            toDoServiceMock.Setup(x => x.FindAll()).Returns(coll);

            IHttpActionResult actual = sut.Get();

            Assert.That(actual, Is.InstanceOf<OkNegotiatedContentResult<CollectionResponseDto<ToDoDto>>>());

            var readableActual = (OkNegotiatedContentResult<CollectionResponseDto<ToDoDto>>)actual;

            Assert.That(readableActual.Content, Is.Not.Null);
            Assert.That(readableActual.Content.Item, Is.EqualTo(coll));
        }

        [Test]
        public void Save__ReturnsOkResponseWithContentFromSave()
        {
            ToDoDto inputDto = new ToDoDto();
            ToDoDto outputDto = new ToDoDto();

            toDoServiceMock.Setup(x => x.Save(inputDto)).Returns(outputDto);

            IHttpActionResult actual = sut.Save(inputDto);

            var readableActual = (OkNegotiatedContentResult<ResponseDto<ToDoDto>>)actual;

            Assert.That(readableActual.Content, Is.Not.Null);
            Assert.That(readableActual.Content.Item, Is.SameAs(outputDto));
        }

        [Test]
        public void Delete__ReturnsOkResponseWithContentFromDelete([Values] bool expected)
        {
            int id = Random.Next();

            toDoServiceMock.Setup(x => x.Delete(id)).Returns(expected);

            IHttpActionResult actual = sut.Delete(id);

            var readableActual = (OkNegotiatedContentResult<ResponseDto<bool>>)actual;

            Assert.That(readableActual.Content, Is.Not.Null);
            Assert.That(readableActual.Content.Item, Is.EqualTo(expected));
        }
    }
}
